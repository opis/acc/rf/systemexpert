<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-10-11 10:28:31 by rafaelmontano-->
<display version="2.0.0">
  <name>$(PREFIX):$(SGNL) Help</name>
  <width>750</width>
  <height>400</height>
  <widget type="label" version="2.0.0">
    <name>Label_322</name>
    <text>PS_OK SIGNAL, The following faults will set the PS_OK to Fault state:
  - OTP
  - OVP
  - Foldback
  - AC fail
  - Enable/Disable open (Power supply is disabled)
  - SO (Rear panel Shut-Off - Power supply is shut off))
  - IEEE failure (with optional IEEE interface)
  - Output Off</text>
    <x>260</x>
    <y>10</y>
    <width>470</width>
    <height>239</height>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group_37</name>
    <x>10</x>
    <y>10</y>
    <width>237</width>
    <height>24</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Label_272</name>
      <text>PSU OK Status</text>
      <width>140</width>
      <height>24</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="group" version="3.0.0">
      <name>Group_38</name>
      <macros>
        <MACRO>TEST</MACRO>
      </macros>
      <x>146</x>
      <width>90</width>
      <height>24</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="rectangle" version="2.0.0">
        <name>Rectangle_87</name>
        <width>90</width>
        <height>24</height>
        <line_width>1</line_width>
        <line_color>
          <color name="BLACK-BORDER" red="121" green="121" blue="121">
          </color>
        </line_color>
        <background_color>
          <color name="Read_Background" red="230" green="235" blue="232">
          </color>
        </background_color>
        <corner_width>3</corner_width>
        <corner_height>3</corner_height>
        <rules>
          <rule name="colorRule" prop_id="background_color" out_exp="false">
            <exp bool_exp="pv0 == 1 &amp;&amp; pv1 == 0">
              <value>
                <color name="ON" red="70" green="255" blue="70">
                </color>
              </value>
            </exp>
            <exp bool_exp="pv0 == 0 &amp;&amp; pv1 == 0">
              <value>
                <color name="OFF" red="90" green="110" blue="90">
                </color>
              </value>
            </exp>
            <exp bool_exp="pv1 == 1">
              <value>
                <color name="ATTENTION" red="252" green="242" blue="17">
                </color>
              </value>
            </exp>
            <pv_name>$(PREFIX):$(SGNL):OK-RB</pv_name>
            <pv_name>$(PREFIX):$(SGNL):FRC-RB</pv_name>
          </rule>
        </rules>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update_428</name>
        <pv_name>$(PREFIX):$(SGNL):OK-RB</pv_name>
        <width>90</width>
        <height>24</height>
        <foreground_color>
          <color name="BLACK" red="0" green="0" blue="0">
          </color>
        </foreground_color>
        <transparent>true</transparent>
        <format>6</format>
        <precision>2</precision>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <rules>
          <rule name="alarmRule" prop_id="background_color" out_exp="false">
            <exp bool_exp="pv0==1">
              <value>
                <color name="MAJOR" red="252" green="13" blue="27">
                </color>
              </value>
            </exp>
            <exp bool_exp="pv0==0">
              <value>
                <color name="OK" red="61" green="216" blue="61">
                </color>
              </value>
            </exp>
            <pv_name>$(pv_name)</pv_name>
          </rule>
        </rules>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group_39</name>
    <x>10</x>
    <y>260</y>
    <width>237</width>
    <height>24</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Label_273</name>
      <text>PSU Ready Status</text>
      <width>140</width>
      <height>24</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="group" version="3.0.0">
      <name>Group_41</name>
      <macros>
        <MACRO>TEST</MACRO>
      </macros>
      <x>146</x>
      <width>90</width>
      <height>24</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="rectangle" version="2.0.0">
        <name>Rectangle_89</name>
        <width>90</width>
        <height>24</height>
        <line_width>1</line_width>
        <line_color>
          <color name="BLACK-BORDER" red="121" green="121" blue="121">
          </color>
        </line_color>
        <background_color>
          <color name="Read_Background" red="230" green="235" blue="232">
          </color>
        </background_color>
        <corner_width>3</corner_width>
        <corner_height>3</corner_height>
        <rules>
          <rule name="colorRule" prop_id="background_color" out_exp="false">
            <exp bool_exp="pv0 == 1">
              <value>
                <color name="ON" red="70" green="255" blue="70">
                </color>
              </value>
            </exp>
            <exp bool_exp="pv0 == 0">
              <value>
                <color name="OFF" red="90" green="110" blue="90">
                </color>
              </value>
            </exp>
            <pv_name>$(PREFIX):$(SGNL):Ready-RB</pv_name>
          </rule>
        </rules>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update_430</name>
        <pv_name>$(PREFIX):$(SGNL):Ready-RB</pv_name>
        <width>90</width>
        <height>24</height>
        <foreground_color>
          <color name="BLACK" red="0" green="0" blue="0">
          </color>
        </foreground_color>
        <transparent>true</transparent>
        <format>6</format>
        <precision>2</precision>
        <horizontal_alignment>1</horizontal_alignment>
        <vertical_alignment>1</vertical_alignment>
        <rules>
          <rule name="alarmRule" prop_id="background_color" out_exp="false">
            <exp bool_exp="pv0==1">
              <value>
                <color name="MAJOR" red="252" green="13" blue="27">
                </color>
              </value>
            </exp>
            <exp bool_exp="pv0==0">
              <value>
                <color name="OK" red="61" green="216" blue="61">
                </color>
              </value>
            </exp>
            <pv_name>$(pv_name)</pv_name>
          </rule>
        </rules>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
    </widget>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_323</name>
    <text>PSU Ready is a combination of ON Status, OK and after Blindout time is elapsed.
After these above conditions are met the State Machine is notified and allow to continue the process.</text>
    <x>260</x>
    <y>260</y>
    <width>480</width>
    <height>100</height>
  </widget>
</display>
