<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-10-17 08:40:02 by benjaminbolling-->
<display version="2.0.0">
  <name>embedded_mbl</name>
  <width>1160</width>
  <height>590</height>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-titlebar_1</name>
    <x>240</x>
    <y>190</y>
    <width>220</width>
    <height>400</height>
    <line_width>0</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>5</corner_width>
    <corner_height>5</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-titlebar_2</name>
    <x>480</x>
    <y>190</y>
    <width>220</width>
    <height>400</height>
    <line_width>0</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>5</corner_width>
    <corner_height>5</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-titlebar_3</name>
    <x>720</x>
    <y>190</y>
    <width>220</width>
    <height>400</height>
    <line_width>0</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>5</corner_width>
    <corner_height>5</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-titlebar_4</name>
    <y>190</y>
    <width>220</width>
    <height>400</height>
    <line_width>0</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>5</corner_width>
    <corner_height>5</corner_height>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BGGrey01-title</name>
    <text>Interlocks</text>
    <y>190</y>
    <width>220</width>
    <height>40</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BGGrey01-title_1</name>
    <text>LLRF</text>
    <x>240</x>
    <y>190</y>
    <width>220</width>
    <height>40</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BGGrey01-title_2</name>
    <text>Amplifiers</text>
    <x>480</x>
    <y>190</y>
    <width>220</width>
    <height>40</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BGGrey01-title_3</name>
    <text>Arc Detectors</text>
    <x>720</x>
    <y>190</y>
    <width>220</width>
    <height>40</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>overview</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(overview_path)</file>
        <macros>
          <CAV_LPS_LINK>$(overview_CAV_LPS_LINK=false)</CAV_LPS_LINK>
          <CAV_LPS_OPI>$(overview_CAV_LPS_OPI=none.bob)</CAV_LPS_OPI>
          <PSSWG1>$(overview_PSSWG1)</PSSWG1>
          <PSSWG2>$(overview_PSSWG2)</PSSWG2>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>System Overview</text>
    <x>20</x>
    <y>50</y>
    <width>400</width>
    <height>120</height>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>section_overview</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(section_overview_path)</file>
        <target>tab</target>
      </action>
    </actions>
    <text>Section Overview</text>
    <x>510</x>
    <y>50</y>
    <width>400</width>
    <height>120</height>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>arcdetectors</name>
    <actions>
      <action type="open_display">
        <description>Arc Detector 1</description>
        <file>$(arcdetectors_path)</file>
        <target>tab</target>
      </action>
      <action type="open_display">
        <description>Arc Detector 2</description>
        <file>$(arcdetectors_path)</file>
        <target>tab</target>
      </action>
    </actions>
    <text>Arc Detectors</text>
    <x>750</x>
    <y>240</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>klystron</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(klystron_path)</file>
        <macros>
          <P>$(klystron_P)</P>
          <R>$(klystron_R)</R>
          <path>$(klystron_path)</path>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>Klystron Information</text>
    <x>510</x>
    <y>280</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>circulator</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(circulator_path)</file>
        <macros>
          <P>$(circulator_P)</P>
          <R>$(circulator_R)</R>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>Circulator</text>
    <x>510</x>
    <y>480</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>filamentpower</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(filamentpower_path)</file>
        <macros>
          <P>$(filamentpower_P)</P>
          <R>$(filamentpower_R)</R>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>Filament</text>
    <x>510</x>
    <y>440</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>ionpump</name>
    <actions>
      <action type="open_display">
        <description>Ion Pump 1</description>
        <file>$(ionpump_path)</file>
        <macros>
          <P>$(rflps_P)</P>
          <R>$(ionpump_R1)</R>
        </macros>
        <target>tab</target>
      </action>
      <action type="open_display">
        <description>Ion Pump 2</description>
        <file>$(ionpump_path)</file>
        <macros>
          <P>$(rflps_P)</P>
          <R>$(ionpump_R2)</R>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>Ion Pump</text>
    <x>510</x>
    <y>400</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>solenoid</name>
    <actions>
      <action type="open_display">
        <description>Solenoid 1</description>
        <file>$(solenoid_path)</file>
        <macros>
          <DeviceName>$(rflps_P)RFS-PSSol-$(rflps_KLY)10</DeviceName>
          <RFCELL>$(rflps_P)</RFCELL>
        </macros>
        <target>tab</target>
      </action>
      <action type="open_display">
        <description>Solenoid 2</description>
        <file>$(solenoid_path)</file>
        <macros>
          <DeviceName>$(rflps_P)RFS-PSSol-$(rflps_KLY)20</DeviceName>
          <RFCELL>$(rflps_P)</RFCELL>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>Solenoid</text>
    <x>510</x>
    <y>360</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>preamp</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(preamp_path)</file>
        <target>tab</target>
      </action>
    </actions>
    <text>Preamp</text>
    <x>510</x>
    <y>320</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>lo</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(lo_path)</file>
        <macros>
          <P>$(lo_P)</P>
          <R>$(lo_R)</R>
          <SYSNAME>$(lo_SYSNAME)</SYSNAME>
          <path>$(lo_path)</path>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>Local Oscillator</text>
    <x>270</x>
    <y>280</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>timing</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(timing_path)</file>
        <macros>
          <EVRPREFIX>$(timing_EVRPREFIX)</EVRPREFIX>
          <MAIN>$(timing_MAIN)</MAIN>
          <PREFIX>$(timing_PREFIX)</PREFIX>
          <SYSNAME>$(timing_SYSNAME)</SYSNAME>
          <path>$(timing_path)</path>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>Timing</text>
    <x>270</x>
    <y>320</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>llrf</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(llrf_path)</file>
        <macros>
          <CAVFLDDIG>$(llrf_CAVFLDDIG=$(PD)$(RD))</CAVFLDDIG>
          <FFCOMP>$(llrf_FFCOMP)</FFCOMP>
          <FIMPV>$(llrf_FIMPV)</FIMPV>
          <HAS2>$(llrf_HAS2=false)</HAS2>
          <HAS3>$(llrf_HAS3=false)</HAS3>
          <HAS4>$(llrf_HAS4=false)</HAS4>
          <IOCNAME>$(llrf_IOCNAME)</IOCNAME>
          <KIDX>$(llrf_KIDX)</KIDX>
          <P>$(llrf_P)</P>
          <PC>$(llrf_PC)</PC>
          <PD>$(llrf_PD)</PD>
          <PR>$(llrf_PR)</PR>
          <PREAMPCH>$(llrf_PREAMPCH=2)</PREAMPCH>
          <PREAMPDIG>$(llrf_PREAMPDIG=$(PD)$(RD))</PREAMPDIG>
          <PREFIX>$(llrf_PREFIX)</PREFIX>
          <PWRAMPCH>$(llrf_PWRAMPCH=3)</PWRAMPCH>
          <PWRAMPDIG>$(llrf_PWRAMPDIG=$(PD)$(RD))</PWRAMPDIG>
          <RC>$(llrf_RC)</RC>
          <RD>$(llrf_RD)</RD>
          <RD1>$(llrf_RD1)</RD1>
          <RD2>$(llrf_RD2)</RD2>
          <RD3>$(llrf_RD3)</RD3>
          <RD4>$(llrf_RD4)</RD4>
          <RR>$(llrf_RR)</RR>
          <RR1>$(llrf_RR1)</RR1>
          <RR2>$(llrf_RR2)</RR2>
          <RR3>$(llrf_RR3)</RR3>
          <RR4>$(llrf_RR4)</RR4>
          <SPRAMP>$(llrf_SPRAMP)</SPRAMP>
          <SYSDESC>$(llrf_SYSDESC)</SYSDESC>
          <SYSNAME>$(llrf_SYSNAME)</SYSNAME>
          <VMDIG>$(llrf_VMDIG=$(PD)$(RD))</VMDIG>
          <path>$(llrf_path)</path>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>LLRF</text>
    <x>270</x>
    <y>240</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>pindiode</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(pindiode_path)</file>
        <macros>
          <P>$(pindiode_P)</P>
          <R>$(pindiode_R)</R>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>PIN Diode</text>
    <x>510</x>
    <y>240</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>electronpickup</name>
    <actions>
      <action type="open_display">
        <description>E-Pickup 1</description>
        <file>$(electronpickup_path)</file>
        <macros>
          <P>$(electronpickup_P)</P>
          <R>$(electronpickup_R)</R>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>Electron Pickup</text>
    <x>30</x>
    <y>280</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>rflps</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(rflps_path)</file>
        <macros>
          <AI00>$(rflps_AI00)</AI00>
          <AI01>$(rflps_AI01)</AI01>
          <AI02>$(rflps_AI02)</AI02>
          <AI03>$(rflps_AI03)</AI03>
          <AI04>$(rflps_AI04)</AI04>
          <AI05>$(rflps_AI05)</AI05>
          <AI06>$(rflps_AI06)</AI06>
          <AI07>$(rflps_AI07)</AI07>
          <AI08>$(rflps_AI08)</AI08>
          <AI09>$(rflps_AI09)</AI09>
          <AI10>$(rflps_AI10)</AI10>
          <AI11>$(rflps_AI11)</AI11>
          <AI12>$(rflps_AI12)</AI12>
          <AI13>$(rflps_AI13)</AI13>
          <AI14>$(rflps_AI14)</AI14>
          <AI15>$(rflps_AI15)</AI15>
          <AI16>$(rflps_AI16)</AI16>
          <AI17>$(rflps_AI17)</AI17>
          <AI18>$(rflps_AI18)</AI18>
          <AI19>$(rflps_AI19)</AI19>
          <CD0>$(rflps_CD0)</CD0>
          <CD1>$(rflps_CD1)</CD1>
          <DI00>$(rflps_DI00)</DI00>
          <DI01>$(rflps_DI01)</DI01>
          <DI02>$(rflps_DI02)</DI02>
          <DI03>$(rflps_DI03)</DI03>
          <DI04>$(rflps_DI04)</DI04>
          <DI05>$(rflps_DI05)</DI05>
          <DI06>$(rflps_DI06)</DI06>
          <DI07>$(rflps_DI07)</DI07>
          <DI08>$(rflps_DI08)</DI08>
          <DI09>$(rflps_DI09)</DI09>
          <DI10>$(rflps_DI10)</DI10>
          <DI11>$(rflps_DI11)</DI11>
          <DI12>$(rflps_DI12)</DI12>
          <DI13>$(rflps_DI13)</DI13>
          <DI14>$(rflps_DI14)</DI14>
          <DI15>$(rflps_DI15)</DI15>
          <DI16>$(rflps_DI16)</DI16>
          <DI17>$(rflps_DI17)</DI17>
          <DI18>$(rflps_DI18)</DI18>
          <DI19>$(rflps_DI19)</DI19>
          <DI20>$(rflps_DI20)</DI20>
          <FIM_MACROS>$(rflps_FIM_MACROS)</FIM_MACROS>
          <IOCSTATS_>$(rflps_IOCSTATS_)</IOCSTATS_>
          <IOCSTATS_FIM>$(rflps_IOCSTATS_FIM)</IOCSTATS_FIM>
          <IOCSTATS_SIM>$(rflps_IOCSTATS_SIM)</IOCSTATS_SIM>
          <IOC_>$(rflps_IOC_)</IOC_>
          <IOC_FIM>$(rflps_IOC_FIM)</IOC_FIM>
          <IOC_SSPA>$(rflps_IOC_SSPA)</IOC_SSPA>
          <KLY>$(rflps_KLY)</KLY>
          <P>$(rflps_P)</P>
          <PREFIX>$(rflps_PREFIX)</PREFIX>
          <P_>$(rflps_P_)</P_>
          <RP0>$(rflps_RP0)</RP0>
          <RP1>$(rflps_RP1)</RP1>
          <R_FIM>$(rflps_R_FIM)</R_FIM>
          <R_FIM_>$(rflps_R_FIM_)</R_FIM_>
          <R_SSPA>$(rflps_R_SSPA)</R_SSPA>
          <R_SSPA_>$(rflps_R_SSPA_)</R_SSPA_>
          <SEC>$(rflps_SEC)</SEC>
          <SUB>$(rflps_SUB)</SUB>
          <path>$(rflps_path)</path>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>LPS Detailed</text>
    <x>30</x>
    <y>240</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>$(timing_SYSNAME)</text>
    <x>20</x>
    <y>10</y>
    <width>400</width>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>ipmimanager</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(ipmimanager_path)</file>
        <macros>
          <CRATE_NUM>$(ipmimanager_CRATE_NUM)</CRATE_NUM>
          <IOCNAME>$(ipmimanager_IOCNAME)</IOCNAME>
          <IOC_PREF>$(ipmimanager_IOC_PREF)</IOC_PREF>
          <MTCA_PREF>$(ipmimanager_MTCA_PREF)</MTCA_PREF>
          <NAME_MODE>$(ipmimanager_NAME_MODE)</NAME_MODE>
          <P>$(ipmimanager_P)</P>
          <SLOT10_IDX>$(ipmimanager_SLOT10_IDX)</SLOT10_IDX>
          <SLOT10_MODULE>$(ipmimanager_SLOT10_MODULE)</SLOT10_MODULE>
          <SLOT11_IDX>$(ipmimanager_SLOT11_IDX)</SLOT11_IDX>
          <SLOT11_MODULE>$(ipmimanager_SLOT11_MODULE)</SLOT11_MODULE>
          <SLOT12_IDX>$(ipmimanager_SLOT12_IDX)</SLOT12_IDX>
          <SLOT12_MODULE>$(ipmimanager_SLOT12_MODULE)</SLOT12_MODULE>
          <SLOT17_MODULE>$(ipmimanager_SLOT17_MODULE)</SLOT17_MODULE>
          <SLOT18_MODULE>$(ipmimanager_SLOT18_MODULE)</SLOT18_MODULE>
          <SLOT19_MODULE>$(ipmimanager_SLOT19_MODULE)</SLOT19_MODULE>
          <SLOT1_IDX>$(ipmimanager_SLOT1_IDX)</SLOT1_IDX>
          <SLOT1_MODULE>$(ipmimanager_SLOT1_MODULE)</SLOT1_MODULE>
          <SLOT20_MODULE>$(ipmimanager_SLOT20_MODULE)</SLOT20_MODULE>
          <SLOT21_MODULE>$(ipmimanager_SLOT21_MODULE)</SLOT21_MODULE>
          <SLOT22_MODULE>$(ipmimanager_SLOT22_MODULE)</SLOT22_MODULE>
          <SLOT23_MODULE>$(ipmimanager_SLOT23_MODULE)</SLOT23_MODULE>
          <SLOT24_MODULE>$(ipmimanager_SLOT24_MODULE)</SLOT24_MODULE>
          <SLOT25_MODULE>$(ipmimanager_SLOT25_MODULE)</SLOT25_MODULE>
          <SLOT26_MODULE>$(ipmimanager_SLOT26_MODULE)</SLOT26_MODULE>
          <SLOT27_MODULE>$(ipmimanager_SLOT27_MODULE)</SLOT27_MODULE>
          <SLOT28_MODULE>$(ipmimanager_SLOT28_MODULE)</SLOT28_MODULE>
          <SLOT2_IDX>$(ipmimanager_SLOT2_IDX)</SLOT2_IDX>
          <SLOT2_MODULE>$(ipmimanager_SLOT2_MODULE)</SLOT2_MODULE>
          <SLOT3_IDX>$(ipmimanager_SLOT3_IDX)</SLOT3_IDX>
          <SLOT3_MODULE>$(ipmimanager_SLOT3_MODULE)</SLOT3_MODULE>
          <SLOT4_IDX>$(ipmimanager_SLOT4_IDX)</SLOT4_IDX>
          <SLOT4_MODULE>$(ipmimanager_SLOT4_MODULE)</SLOT4_MODULE>
          <SLOT5_IDX>$(ipmimanager_SLOT5_IDX)</SLOT5_IDX>
          <SLOT5_MODULE>$(ipmimanager_SLOT5_MODULE)</SLOT5_MODULE>
          <SLOT6_IDX>$(ipmimanager_SLOT6_IDX)</SLOT6_IDX>
          <SLOT6_MODULE>$(ipmimanager_SLOT6_MODULE)</SLOT6_MODULE>
          <SLOT7_IDX>$(ipmimanager_SLOT7_IDX)</SLOT7_IDX>
          <SLOT7_MODULE>$(ipmimanager_SLOT7_MODULE)</SLOT7_MODULE>
          <SLOT8_IDX>$(ipmimanager_SLOT8_IDX)</SLOT8_IDX>
          <SLOT8_MODULE>$(ipmimanager_SLOT8_MODULE)</SLOT8_MODULE>
          <SLOT9_IDX>$(ipmimanager_SLOT9_IDX)</SLOT9_IDX>
          <SLOT9_MODULE>$(ipmimanager_SLOT9_MODULE)</SLOT9_MODULE>
          <path>$(ipmimanager_path)</path>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>IPMI Manager</text>
    <x>270</x>
    <y>535</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>modulator</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(modulator_path)</file>
        <macros>
          <P>$(modulator_P)</P>
          <R>$(modulator_R)</R>
          <PSS_PV>$(modulator_PSS_PV)</PSS_PV>
          <MODULATOR_UNIT>$(modulator_DESC)</MODULATOR_UNIT>
          <IOCSTATS_PREFIX>$(modulator_IOCSTATS_PREFIX)</IOCSTATS_PREFIX>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>Modulator</text>
    <x>510</x>
    <y>520</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>oscilloscope</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>$(oscilloscope_path)</file>
        <macros>
          <P>$(oscilloscope_P)</P>
          <R>$(oscilloscope_R)</R>
          <RFCELL>$(oscilloscope_RFCELL)</RFCELL>
          <EVR_P>$(timing_EVRPREFIX)</EVR_P>
          <EVR_R></EVR_R>
        </macros>
        <target>tab</target>
      </action>
    </actions>
    <text>Oscilloscope</text>
    <x>30</x>
    <y>320</y>
    <width>160</width>
    <tooltip>$(actions)</tooltip>
    <enabled>$($(name)_enabled)</enabled>
  </widget>
</display>
